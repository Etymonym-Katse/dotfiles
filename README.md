# NixOS dotfiles
This is where I store my -- under development -- dotfiles for managing NixOS!

## Inspiration
The list below is an incomplete record of sources of inspiration for the contents of this repo -- it will likely grow:
### [Wil Taylor](https://github.com/wiltaylor)
  - The initial setup of this repository was nearly entirely structured after the [extremely helpful tutorials](https://www.youtube.com/playlist?list=PL-saUBvIJzOkjAw_vOac75v-x6EzNzZq-) over at [Wil's Channel](https://www.youtube.com/@wilfridtaylor) on Youtube. You can also find [his dotfiles here](https://github.com/wiltaylor/dotfiles) if you are interested in his -- significantly more sophisticated, (at least at the moment) -- dotfiles setup.

## License
This repository and its contents are licensed under Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0).
