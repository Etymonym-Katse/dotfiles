#!/bin/sh
pushd ~/.dotfiles
mv ./users/result ./users/oldResult
nix build .#homeManagerConfigurations.katse.activationPackage
mv result ./users/result
./users/result/activate
rm ./users/oldResult
popd
