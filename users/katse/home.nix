{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "katse";
  home.homeDirectory = "/home/katse";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "22.11";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  programs.gpg = {
    enable = true;
  };

  services.gpg-agent = {
    enable = true;
    pinentryFlavor = "qt";
  };

  programs.ssh = {
    enable = true;
    matchBlocks = {
      "gitlab.com" = {
        identityFile = "~/.ssh/id_gitlab";
        user = "git";
      };
      #"github.com" = {
      #  identityFile = "~/.ssh/id_github";
      #  user = "git";
      #};
    };
  };

  home.packages = with pkgs; [
    # Git
    git
    git-crypt

    # GPG
    gnupg
    pinentry_qt

    # Internet
    firefox

    # Messaging
    element-desktop

    # Development
    python3

    # Utility
    xclip
  ];

  home.file = {
    #".config/application/application.config".text = ''
    #configuration stuff
    #'';
  };

}

