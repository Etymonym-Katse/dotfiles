{
  description = "My System Configuration ( Most Likely )";

  inputs = { 
    nixpkgs.url = "nixpkgs/master";
    home-manager.url = "github:nix-community/home-manager/master";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { nixpkgs, home-manager, ... }:
  let 
    system = "x86_64-linux";

    pkgs = import nixpkgs {
      inherit system;
      config = { allowUnfree = true; };
    };

    lib = nixpkgs.lib;

  in {
    homeManagerConfigurations = {
      katse = home-manager.lib.homeManagerConfiguration {
        inherit pkgs;
        
        modules = [
            ./users/katse/home.nix
          ];
      };
    };


    nixosConfigurations = {
      creomagnus = lib.nixosSystem {
       inherit system;
 
        modules = [
          ./system/configuration.nix
        ];
      };
    };
  };
}
